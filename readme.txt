v0.3.1:
  - cu110
  - torch170 (nvcr.io/nvidia/pytorch:20.09-py3)
  - v0.3.2에는 pytorch3d와 pytorch-lightning이 설치되어있음

- wandb 설치하지 않은 이유: msgpack version에서 충돌남. 실행 자체에 문제는 없어 보이나 만약을 위해 제거하고 build함.
